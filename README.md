# FalaBrasil Official Static Site with Hugo Framework

https://ufpafalabrasil.gitlab.io/

## Instructions to set up a Hugo environment on Gitlab Pages

- clone Beautiful Hugo (BH) template from https://github.com/halogenica/beautifulhugo/
    - `git clone https://github.com/halogenica/beautifulhugo.git`
- create an empty repo at https://gitlab.com/ufpafalabrasil/ufpafalabrasil.gitlab.io via browser
- clone it
    - `git clone https://gitlab.com/ufpafalabrasil/ufpafalabrasil.gitlab.io`
- copy __the contents__ of the folder `exampleSite` from BH repo into your falabrasil repo 
    - `cp -rv beautifulhugo/exampleSite/* ufpafalabrasil/`
- create your `.gitlab-ci.yml` configuration file
    - copy and paste: https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/#create-gitlab-ciyml
- udpate your hugo docker image to `hugo_extended:latest`
    - see https://gitlab.com/pages/hugo/container_registry
    - `image: registry.gitlab.com/pages/hugo/hugo_extended:latest`
- update `baseurl` param in `config.toml` file to point to your namespace
    - `baseurl = "https://ufpafalabrasil.gitlab.io"`
- add gitlab conf file, BH conf file and contents dir to git staging area
- push to origin


[![FalaBrasil](doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2020)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Cassio Batista - https://cassota.gitlab.io/    
