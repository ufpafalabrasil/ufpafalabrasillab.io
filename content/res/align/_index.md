---
title: Alinhamento Fonético
subtitle: Recursos livres para alinhar áudio e texto em Português Brasileiro
author: Cassio Batista
author_link: https://cassota.gitlab.io/
date: 2019-08-01
lastmod: 2019-08-14
type: page
comments: false
bigimg: [{src: "/img/aligner-big-img.gif", desc: "alinhamento fonético"}]
---

# Apresentação
O UFPAlign é um ferramenta para alinhamento automático de áudio e texto em Português Brasileiro desenvolvido pelo Grupo FalaBrasil. O alinhador foi desenvolvido e livremente disponibilizado como um _plugin_ para o Praat, _software_ para a análise de fala.

Este tutorial foi criado com o intuito de instruir o usuário quanto a
instalação e utilização do UFPAlign.   

## Sumário
1. [Apresentação](/res/align/)
2. [Instalação](/res/align/02_install/)
3. [Utilização](/res/align/03_init_screen/)
