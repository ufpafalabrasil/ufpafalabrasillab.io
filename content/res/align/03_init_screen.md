---
title: "Utilização"
subtitle: UFPAlign Tutorial
author: Cassio Batista
author_link: https://cassota.gitlab.io/
date: 2019-08-10
lastmod: 2019-08-14
type: page
weight: 3
comments: false
#bigimg: [{src: "/img/vox.jpeg", desc: "VoxLaPS: Um Vocalizador Aberto para Plataformas Móveis"}]
---

Tela inicial do aplicativo.
<!--more-->

Para abrir o plugin no Praat, abra o menu “New” e clique na opção “UFPAlign...”, a seguinte janela será
exibida:    

![](/img/ufpalign_1.png)


Clique nos botões “Procurar...” para selecionar os arquivos de áudio e as transcrições.    
Após selecioná-los clique em “Alinhar”. Aguarde enquanto o arquivo é alinhado. Quando o
alinhamento for concluído, a seguinte janela será exibida:

![](/img/ufpalign_2.png)

Clique em “Não” caso não queira visualizar no Praat seu arquivo TextGrid criado. Se quiser
visualiza-lo clique em “Sim”. A figura abaixo mostra um alinhamento feito pelo UFPAlign.    

![](/img/aligner-big-img.gif) 

__IMPORTANTE__: Os arquivos de áudio e de texto devem possuir o mesmo nome, com suas
extensões “.wav” e “.txt”. O arquivo de áudio deve estar amostrado em uma frequência de 16 Khz
com 1 canal.    
