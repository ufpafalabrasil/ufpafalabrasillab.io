---
title: "Instalação"
subtitle: UFPAlign Tutorial
author: Cassio Batista
author_link: https://cassota.gitlab.io/
date: 2019-08-10
lastmod: 2019-08-10
type: page
weight: 2
comments: false
#bigimg: [{src: "/img/vox.jpeg", desc: "VoxLaPS: Um Vocalizador Aberto para Plataformas Móveis"}]
---

Aqui você encontra instruções sobre como instalar a ferramenta UFPAlign.
<!--more-->

### Requisitos
- HTK toolkit   
- Python2.7   
- Praat   

### Instalação manual
- Realize o _download_ da versão da ferramenta correspondente ao seu sistema operacional no repositório https://gitlab.com/fb-align/hdecode-align.    
- Salve a pasta dentro do diretório .praat-dir na sua home.  
- Reinicie o Praat.   

Após concluída a instalação, o UFPAlign poderá ser encontrado na aba '_New_' do _software_ Praat. Daí é só aproveitar ;D
