---
title: "Processamento de Linguagem Natural"
subtitle: "Recursos livres para NLP em Português Brasileiro"
author: Cassio Batista
author_link: https://cassota.gitlab.io/
date: 2019-08-01
lastmod: 2019-09-04
type: page
comments: false
bigimg: [{src: "/img/nlp-big-img.jpg", desc: "processamento de linguagem natural"}]
---

## NLP: processamento de linguagem natural
![](/img/nlp.png)
Coming soon.

https://gitlab.com/fb-nlp

## Recursos prontos
https://gitlab.com/fb-nlp/nlp-resources

## Ferramentas para geração de recursos
https://gitlab.com/fb-nlp/nlp-generator
