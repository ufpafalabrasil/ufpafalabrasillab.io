# Índice para os Repositórios do GitLab
- [Corpora de Áudio Transcrito](https://gitlab.com/fb-audio-corpora) 
- [Corpora de Texto](https://gitlab.com/fb-text-corpora) 
- [ASR: Reconhecimento Automático de Fala](https://gitlab.com/fb-asr) 
    - [Recursos prontos](https://gitlab.com/fb-asr/fb-asr-resources)
    - [AM: Tutoriais para treino de modelos acústicos](https://gitlab.com/fb-asr/fb-am-tutorial)
    - [LM: Tutoriais para treino de modelos de linguagem](https://gitlab.com/fb-asr/fb-lm-tutorial)
- [TTS: Síntese de Fala](https://gitlab.com/fb-tts) 
- [NLP: Processamento de Linguagem Natural](https://gitlab.com/fb-nlp)
- [Alinhamento Fonético](https://gitlab.com/fb-align)
- [Aplicações em Fala](https://gitlab.com/fb-apps)

#### Repositório oficial: https://gitlab.com/falabrasil
