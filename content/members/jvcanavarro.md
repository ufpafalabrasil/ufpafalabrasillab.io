---
short_name:     "João Canavarro"
full_name:      "João Victor da Silva Dias Canavarro"
role:           "Graduando"
src_img:        "/img/jvcanavarro.jpg"
orcid:          "0000-0002-0700-8123"
email:          "jvcanavarro@gmail.com"
lattes:         "1994255852377245"
linkedin:       "jvcanavarro"
github:         "jvcanavarro"
gitlab:         "jvcanavarro"
---

Estudante de graduação em Ciência da Computação (2017 - Hoje) na Universidade Federal do Pará, UFPA. Sua experiência profissional inclui desenvolvimento de ferramentas para pré-processamento de dados genéticos.
