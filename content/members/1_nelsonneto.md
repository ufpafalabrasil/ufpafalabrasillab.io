---
short_name:     "Nelson Neto"
full_name:      "Nelson Cruz Sampaio Neto"
role:           "Professor Adjunto"
src_img:        "/img/nelsonneto.png"
google_scholar: "hA6LXfUAAAAJ"
research_gate:  "Nelson_Neto"
orcid:          "0000-0003-0408-4187"
email:          "nelsonneto@ufpa.br"
lattes:         "9756167788721062"
#linkedin:       "username"
#github:         "username"
gitlab:         "dnelsonneto"

#bitbucket:      "username"
#telegram:       "username"
#keybase:        "username"
#youtube:        "username"
#stackoverflow:  "username"
#twitter:        "username"
#reddit:         "username"
#academia_edu:   "username"
#googleplus:     "username"
#facebook:       "username"
#xing:           "username"
#snapchat:       "username"
#instagram:      "username"
#soundcloud:     "username"
#spotify:        "username"
#bandcamp:       "username"
#itchio:         "username"
#vk:             "username"
#paypal:         "username"
---

Possui graduação em Tecnologia em Processamento de Dados pelo Centro de Ensino
Superior do Pará (1997), graduação (2000), mestrado (2006) e doutorado em 
Engenharia Elétrica (2011) pela Universidade Federal do Pará (UFPA). Realizou 
atividades como Engenheiro de Telecomunicações na Amazônia Celular S.A 
(2001-2007). Atualmente, é Professor Adjunto IV da Faculdade de Computação e do 
Programa de Pós-Graduação em Ciência da Computação da UFPA. Tem experiência nas 
áreas de processamento de linguagem natural, reconhecimento e síntese de fala 
(voz). É integrante do Laboratório de Visualização, Interação e Sistemas 
Inteligentes (LabVIS) e coordenador do Grupo FalaBrasil.
