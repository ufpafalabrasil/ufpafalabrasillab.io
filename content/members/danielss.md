---
short_name:     "Daniel Santana"
full_name:      "Daniel Santos Santana"
role:           "Graduando"
email:          "daniel.santana.1661@gmail.com"
lattes:         "5122788866031012"
github:         "daniel-santana"
gitlab:         "Daniel.SS"
orcid:          "0000-0003-3259-3739"
src_img:        "/img/daniels.jpg"
---

Estudante de graduação em Ciência da Computação (2015-Hoje) na Universidade 
Federal do Pará (UFPA). Trabalhou na criação de um conversor grafema fonema
crossword, e com desenvolvimento back end em node.
