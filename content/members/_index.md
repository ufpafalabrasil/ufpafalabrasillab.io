---
title: Membros
subtitle: Integrantes do Grupo FalaBrasil
author: Cassio Batista
type: tags
layout: member
date: 2019-07-14T11:08:52-03:00
lastmod: 2019-08-06
image: ""
tags: []
comments: false
---

- **Participantes anteriores**
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/1596629769697284) Aldebaro Barreto da Rocha Klautau Jr.
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Ana Carolina Quintão Siravenha
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Carlos Patrick Alves da Silva 
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Chadia Hosn
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Denise Costa Alves
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Ênio dos Santos Silva
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Hugo Santos
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Igor Costa do Couto
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Jefferson Magalhães de Morais
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Joarley Wanzeler de Moraes
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Luiz Albero Novaes
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Pedro dos Santos Batista
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Rafael Oliveira Santana
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Renan Moura Ferreira
  - [<i class="ai ai-lattes"></i>](http://lattes.cnpq.br/) Renan Fonseca Cunha
