---
short_name:     "Cassio Batista"
full_name:      "Cassio Trindade Batista"
role:           "Doutorando"
src_img:        "/img/cassota.jpeg"
google_scholar: "_04j8QUAAAAJ"
research_gate:  "Cassio_Batista"
orcid:          "0000-0001-6799-6058"
email:          "cassiotb@ufpa.br"
lattes:         "2464594478834482"
linkedin:       "cassota"
github:         "cassiobatista"
gitlab:         "cassota"
bitbucket:      "cassota"
telegram:       "cassota"
keybase:        "cassota"
youtube:        "channel/UCWPJAKVyu7Kv4LBk6-ngraQ"
stackoverflow:  "3798300/cassota"
twitter:        "cassiobatista"
reddit:         "cassota"
academia_edu:   "CassioBatista"
link:           "https://cassota.gitlab.io/"
#googleplus:     "username"
#facebook:       "cassota"
#xing:           "username"
#snapchat:       "cassota"
#instagram:      "cassota"
#soundcloud:     "username"
#spotify:        "cassio.batista.13"
#bandcamp:       "username"
#itchio:         "username"
#vk:             "username"
#paypal:         "username"
---

Possui graduação em Engenharia da Computação (2016) e mestrado em Ciência da
Computação (2017), pela Universidade Federal do Pará (UFPA). Foi bolsista
CAPES pelo programa Ciência sem Fronteiras na Universidade de Óbuda (OE) em
Budapeste, Hungria, durante um ano do período da graduação (2014). Atualmente, é
estudante de doutorado pelo Programa de Pós-Graduação em Ciência da Computação
da UFPA. Tem experiência em reconhecimento de fala, e no desenvolvimento de 
interfaces alternativas de controle voltadas para pessoas com deficiência.
