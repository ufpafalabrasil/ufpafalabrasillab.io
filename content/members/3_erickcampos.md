---
short_name:     "Erick Campos"
full_name:      "Erick Modesto Campos"
role:           "Mestrando"
src_img:        "/img/erick_lindo_perfeito.jpg"
#google_scholar: "usercode"
#research_gate:  "username"
#orcid:          "usercode"
email:          "erickcampos@ufpa.br"
lattes:         "9566244868798202"
#linkedin:       "username"
github:         "ErickCampos"
gitlab:         "ErickModesto"
#bitbucket:      "username"
telegram:       "Caboco"
#keybase:        "username"
#youtube:        "username"
#stackoverflow:  "username"
#twitter:        "username"
#reddit:         "username"
#academia_edu:   "username"
#googleplus:     "username"
#facebook:       "username"
#xing:           "username"
#snapchat:       "username"
#instagram:      "username"
#soundcloud:     "username"
#spotify:        "username"
#bandcamp:       "username"
#itchio:         "username"
#vk:             "username"
#paypal:         "username"
---

Graduado em Engenharia da Computação pela Universidade Federal do Pará (2018).
Atualmente é discente de mestrado em Ciência da Computação no Programa de
Pós-Graduação em Ciência da Computação (PPGCC) da Universidade Federal do Pará.
Tem experiência no desenvolvimento de interfaces alternativas de controle
voltadas para pessoas com deficiência; e em reconhecimento de gestos da cabeça
para aplicações em desktop e em sistemas embarcados. Linhas de pesquisa:
tecnologia assistiva, interação humano-computador.
