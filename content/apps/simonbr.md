---
title: SimonBR
subtitle: Substituindo teclado e mouse por comandos de voz
author: Cassio Batista
author_link: https://cassota.gitlab.io/
date: 2019-08-01
lastmod: 2019-08-13
type: page
comments: false
bigimg: [{src: "/img/simon.png", desc: "SimonBR"}]
---

**NOTA**: Aparentemente, o projeto do Simon do KDE foi descontinuado em agosto
de 2015 em sua versão 0.4.1 após o mantenedor, Peter Grasch, em seu blog,
anunciar que iria "[passar a bola](http://grasch.net/node/174)" e indicar o que, 
em sua visão, seriam os "[próximos passos](http://grasch.net/node/192)" para o 
projeto. Portanto, todos os recursos criados pelo Grupo FalaBrasil, enquanto os
membros ainda pertenciam ao antigo Laboratório de Processamento de Sinais
(LaPS), encontram-se atualmente **desatualizados**.

O Simon é um programa _open-source_ idealizado pelo grupo Simon Listens que 
auxilia pessoas com deficiência a usufruir das funcionalidades computacionais 
usando somente comandos de voz. Já o pacote SimonBR disponibiliza modelo
acústico base, dicionário fonético, interfaces traduzidas e cenários específicos 
para o Português Brasileiro.

Cenários são ferramentas criadas para adicionar funcionalidades ao Simon. Eles
são parecidos com o que chamamos de _add-ons_ ou Complementos do Mozilla 
Firefox. Os cenários dispõem de comandos que são recebidos por meio de frases 
pré-definidas e que executam sua função correspondente. Os comandos de voz dos 
cenários foram criados de forma a serem chamados intuitivamente, ou seja, 
economizam palavras para tornar mais confortável o uso.

### Demonstração
- Vídeo demonstrativo do funcionamento no Ubuntu [aqui](http://www.youtube.com/watch?v=QCRN8y7Y7OI)
- Vídeo demonstrativo do funcionamento no Windows [aqui](http://www.youtube.com/watch?v=57NPYyn6HsM)
- Programa sementes da TV cultura [aqui](http://www.youtube.com/watch?v=dVnB_CH7Sx0)

### Instalação no Ubuntu:
- Simon: https://simon.kde.org/
- Recursos para PT_BR: https://gitlab.com/fb-apps/simonbr
