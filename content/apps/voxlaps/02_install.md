---
title: "Instalação"
subtitle: VoxLaPS Tutorial
author: Ronaldd Pinho
author_link: https://github.com/pinho
date: 2019-08-10
lastmod: 2019-08-10
type: page
weight: 2
comments: false
#bigimg: [{src: "/img/vox.jpeg", desc: "VoxLaPS: Um Vocalizador Aberto para Plataformas Móveis"}]
---

Aqui você encontra instruções sobre como instalar o aplicativo do VoxLaPS no seu
próprio _tablet_ Android.
<!--more-->

### Instalação via Google Play
- Link: https://play.google.com/store/apps/details?id=com.laps.vox

### Instalação manual
- Obtenha o arquivo (.apk) do aplicativo 
- Acesse as configurações do seu dispositivo (aplicativo com o ícone de engrenagem).  
- Já em configurações procure e clique na opção "Segurança".  
- Procure e ative a opção de <b>Fontes desconhecidas</b> e ative caso esteja desativada, 
isso permitirá que o seu dispositivo instale aplicativos que não vêm da Google 
Play Store. Caso prefira, pode desativar esta opção depois que a instalação do 
VoxLaPS estiver concluída.

![](/img/voxlaps_manual/install-config.png)

- Use o seu gerenciador de arquivos para ir até a pasta onde está arquivo do 
aplicativo (.apk).

![](/img/voxlaps_manual/install-diretorio-apk.png)

- Clique no arquivo, e toque em <b>Instalar</b>

![](/img/voxlaps_manual/install-a-instalar.png)

- Caso apareça em seguida, clique em <b>Instalador de pacotes</b> e por fim 
clique em <b>Instalar</b> para iniciar a instalação.

![](/img/voxlaps_manual/install-pacote.png)

![](/img/voxlaps_manual/install-instalar.png)

Após concluída a instalação, o VoxLaPS poderá ser encontrado na grade de 
aplicativos do seu dispositivo. Daí é só aproveitar ;D

![](/img/voxlaps_manual/installing.png)

![](/img/voxlaps_manual/installed.png)
