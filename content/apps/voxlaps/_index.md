---
title: VoxLAPS
subtitle: Vocalizador para comunicação alternativa e aumentativa
author: Ronaldd Pinho
author_link: https://github.com/pinho
date: 2019-08-01
lastmod: 2019-08-07
type: page
comments: false
bigimg: [{src: "/img/vox.jpeg", desc: "VoxLaPS: Um Vocalizador Aberto para Plataformas Móveis"}]
---

# Apresentação
O VoxLaPS é um _software_ vocalizador livre desenvolvido pelo Grupo FalaBrasil, 
dedicado a auxiliar na comunicação de pessoas com deficiência, especialmente na 
fala. O aplicativo permite o cadastro de diferentes usuários identificados com 
um nome e que podem selecionar suas próprias configurações de uso.

Este tutorial foi criado com o intuito de instruir o usuário quanto a
instalação, utilização e configuração do VoxLaPS em dispositivos Android.

## Sumário
1. [Apresentação](/apps/voxlaps/)
2. [Instalação](/apps/voxlaps/02_install/)
3. [Tela Inicial](/apps/voxlaps/03_init_screen/)
4. [Usuário](/apps/voxlaps/04_user/)
5. [Pranchas](/apps/voxlaps/05_boards/)
6. [Figuras](/apps/voxlaps/06_figures/)
7. [Área de Vocalização](/apps/voxlaps/07_vocal_area/)
