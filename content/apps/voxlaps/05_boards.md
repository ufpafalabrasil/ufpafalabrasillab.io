---
title: "Pranchas"
subtitle: VoxLaPS Tutorial
author: Ronaldd Pinho
author_link: https://github.com/pinho
date: 2019-08-10
lastmod: 2019-08-10
type: page
weight: 5
comments: false
#bigimg: [{src: "/img/vox.jpeg", desc: "VoxLaPS: Um Vocalizador Aberto para Plataformas Móveis"}]
---

Uma prancha é um bloco de figuras, uma tabela que contém e organiza as figuras 
por categorias. Existem algumas pranchas padrões do sistemas como: Pessoa, 
Pergunta, Expressão, Sentimento, Verbo, dentre outras.
<!--more-->

![Pranchas](/img/voxlaps_manual/comece-a-falar.png)

### Como criar uma nova prancha?

Na Tela do Usuário, clique no botão **Alterar Pranchas e Figuras**.

Você terá uma tela com todas as pranchas criadas ou editadas pelo usuário 
(escolhido na Tela de Usuários cadastrados) além das opções **Voltar** e 
**Nova Prancha**.

![Tela de Usuários](/img/voxlaps_manual/tela-usuario-voxlaps.png) 
![Tela de alteração vazia](/img/voxlaps_manual/tela-altera-fig-pranchas-joaosilva.png) 

Obs: A tabela de pranchas de um usuário recém criado é vazia.

Clique no botão **Nova Prancha**;

É aberta uma tela onde devem ser inseridas as informações da prancha a ser criada.

!["Tela de alteração vazia"](/img/voxlaps_manual/tela-altera-fig-pranchas-joaosilva.png) 
!["Tela de alteração vazia"](/img/voxlaps_manual/criando-a-prancha-animais.png)          

Digite um nome para a prancha;    
Selecione a quantidade de figuras que serão mostradas em uma só tabela dessa 
prancha através do número de linhas e número de colunas;

!["Tela de alteração vazia"](/img/voxlaps_manual/criando-prancha-animais-nome.png)  
!["Tela de alteração vazia"](/img/voxlaps_manual/criando-a-prancha-animais-tamtabela.png) 

Escolha uma cor para a prancha, essa cor será adicionada em todas as figuras 
que pertencerão à prancha;   
Escolha uma imagem de identificação para a prancha (obrigatório) clicando em:

- Galeria, para selecionar a partir das imagens salvas na memória do seu tablet/celular.
- Câmera, para tirar uma foto e adicionar à prancha.

![Tela de alteração vazia](/img/voxlaps_manual/carregar-imagem.png)
![Tela de alteração vazia](/img/voxlaps_manual/criando-prancha-animais.png) 

Após inserir as informações clique em **Salvar**.

![Prancha Animais criada](/img/voxlaps_manual/prancha-animais-criada.png) 

### Como editar uma prancha?

Na área de **Alteração de pranchas e figuras**, clique na prancha que você deseja editar.

Será mostrada uma tela com as figuras que pertecem à prancha escolhida.

![Editando prancha](/img/voxlaps_manual/prancha-animais-criada.png) 
![Prancha vazia](/img/voxlaps_manual/prancha-animais-vazia.png)  

Clique em **Editar Prancha**.

As informações sobre a prancha aparecerão para serem editadas. É possível:

- Alterar o nome da prancha;
- Alterar a quantidade de figuras que serão mostradas em uma só tabela dessa prancha através do número de linhas e número de colunas;
- Alterar a cor da prancha, que será adicionada a todas as figuras que pertencem à prancha;
- Alterar a imagem da prancha:

Após alterar as informações que deseja clique em **Salvar** para confirmar.

![Prancha vazia](/img/voxlaps_manual/prancha-animais-vazia.png) 
![Editando a prancha animais](/img/voxlaps_manual/editando-prancha-animais-mudando-cor.png) 

Exemplo: Nas imagens abaixo os números de linhas e colunas da prancha foram 
alterados para 4 e 5, respectivamente.

![Editando a prancha animais](/img/voxlaps_manual/editando-prancha-animais-mudando-cor.png) 
![Editando a prancha animais](/img/voxlaps_manual/prancha-animais-vazia-4x5.png) 
