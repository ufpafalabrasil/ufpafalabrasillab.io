---
title: "Área de Vocalização"
subtitle: VoxLaPS Tutorial
author: Ronaldd Pinho
author_link: https://github.com/pinho
date: 2019-08-10
lastmod: 2019-08-10
type: page
weight: 7
comments: false
#bigimg: [{src: "/img/vox.jpeg", desc: "VoxLaPS: Um Vocalizador Aberto para Plataformas Móveis"}]
---

Para usar os recursos de vocalização do VoxLaPS acesse a área de vocalização do 
aplicativo.

<!--more-->

Escolha um dos usuários disponíveis.

Clique no botão <b>Comece a Falar</b> na Tela de Usuário.

![](/img/voxlaps_manual/escolha-user.png)

![](/img/voxlaps_manual/tela-usuario-comece.png)

Será aberta a tela com a tabela de Pranchas do usuário escolhido 
(será uma tabela vazia caso o usuário seja recém-criado).

Clique na prancha a qual a figura que você deseja pertence, e então clique na 
figura desejada.

![](/img/voxlaps_manual/tela-comecar-a-falar-exp-destacada.png)

![](/img/voxlaps_manual/tela-comecar-a-flar-prancha-expressao.png)

Automaticamente o áudio da figura será executado e o texto poderá ser visto 
acima, e pode ser executado de novo a qualquer momento clicando no botão de 
Play (botão triangular no canto superior esquerdo) à esquerda do texto.

Botões e suas respectivas funções:

<table class="tabela">
  <tr>
    <td class="funcao">Função</td>
    <td class="botao">Botão</td>
  </tr>
  <tr>
    <td><p>Apaga uma única palavra do texto por vez.</p></td>
    <td><img class="button" src="/img/voxlaps_manual/backspace.png"/></td>
  </tr>
  <tr>
    <td><p>Apaga o texto inteiro.</p></td>
    <td><img class="button" src="/img/voxlaps_manual/botao-limpar.png"/></td>
  </tr>
  <tr>
    <td><p>Reproduz em áudio o texto que está está selecionado.</p></td>
    <td><img class="button" src="/img/voxlaps_manual/botao-play.png"/></td>
  </tr>
  <tr>
    <td><p>Vai para a próxima página de pranchas, ou de figuras.</p></td>
    <td><img class="button" src="/img/voxlaps_manual/botao-avancar.png"/></td>
  </tr>
  <tr>
    <td><p>Volta para a página de pranchas, ou de figuras, anterior.</p></td>
    <td><img class="button" src="/img/voxlaps_manual/botao-voltar.png"/></td>
  </tr>
</table>
