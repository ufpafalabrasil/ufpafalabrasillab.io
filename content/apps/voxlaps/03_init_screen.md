---
title: "Tela Inicial"
subtitle: VoxLaPS Tutorial
author: Ronaldd Pinho
author_link: https://github.com/pinho
date: 2019-08-10
lastmod: 2019-08-10
type: page
weight: 3
comments: false
#bigimg: [{src: "/img/vox.jpeg", desc: "VoxLaPS: Um Vocalizador Aberto para Plataformas Móveis"}]
---

Tela inicial do aplicativo.
<!--more-->

Ao abrir o aplicativo você encontra esta tela:

![](/img/voxlaps_manual/1_tela_inicial.png)

O botão usuários leva à tela de usuários.

Na primeira vez que o aplicativo é aberto depois de instalado, ao clicar em 
<b>Usuários</b> será necessário dar permissões ao aplicativo selecionando a 
opção "Permitir" na janela que aparece.

![](/img/voxlaps_manual/inicio-bt-user-m.png)
![](/img/voxlaps_manual/pa.png)

Obs1: O aplicativo precisa das permissões para poder criar uma pasta onde 
guardará as imagens usadas. Depois que permitir esse acesso ao aplicativo ele 
permanecerá ativado.

Obs2: Dependendo da vessão do Android o aplicativo pode ativar as permissões 
automaticamente.

Com as permissões concedidas, o aplicativo abre a Tela de Usuários.
