---
title: "Figuras"
subtitle: VoxLaPS Tutorial
author: Ronaldd Pinho
author_link: https://github.com/pinho
date: 2019-08-10
lastmod: 2019-08-10
type: page
weight: 6
comments: false
#bigimg: [{src: "/img/vox.jpeg", desc: "VoxLaPS: Um Vocalizador Aberto para Plataformas Móveis"}]
---

Uma figura é um botão com um texto e uma imagem que representa uma palavra ou 
expressão, quando uma figura é clicada reproduz em áudio a palavra a qual a 
imagem representa.
<!--more-->

![Figuras 1](/img/voxlaps_manual/tela-comecar-a-falar-prancha-adjetivo.png) 
![Figuras 2](/img/voxlaps_manual/tela-comecar-a-falar-prancha-pergunta.png) 

### Como criar uma nova figura?

Vá para a área de **alteração de pranchas e figuras** e escolha a prancha em 
que deseja criar a nova figura.

Clique em **Nova Prancha**.

![](/img/voxlaps_manual/prancha-animais-criada.png)
![](/img/voxlaps_manual/prancha-animais-vazia.png)

Digite um nome para a figura.<br>Esse nome será exibido na figura acima da 
imagem.

Digite o texto da figura que será usado para gerar o áudio da figura, caso 
você não queira gravar seu próprio áudio.

Adicione uma imagem para a figura clicando em:    

- Galeria, para selecionar a partir das imagens salvas na memória do seu tablet/celular.   
- Câmera, para tirar uma foto e adicionar à figura.

![](/img/voxlaps_manual/carregar-imagem.png)

![](/img/voxlaps_manual/criando-figura-cachorro.png)

Caso prefira, grave seu próprio áudio para adicionar à figura:

- Clique em Gravar voz, fale enquanto o botão estiver alaranjado e clique de novo para parar.
- Clique em Ouvir para ouvir o áudio que você gravou.
- Caso queira regravar, refaça o processo clicando em Gravar voz outra vez.

Clique em **Salvar** para confirmar as informações da figura e criar.

![](/img/voxlaps_manual/figura-cachorro-criada.png)

### Como editar uma figura?

Vá para a área de **Alteração de Pranchas e Figuras** e escolha a prancha 
em que deseja alterar a figura.

![](/img/voxlaps_manual/figura-cachorro-criada.png)

É aberta uma tela com as informações já preenchidas da figura. É possível editar:

- O nome que é exibido na figura;
- O texto que é usado para gerar o áudio da figura, caso você não queira gravar seu próprio áudio.
- Caso a figura já tenha um áudio gravado, você pode excluí-lo clicando em Limpar.

Alterar a imagem da figura:

- Clicando em Galeria, seleciona a nova imagem a partir das imagens salvas no seu tablet/celular.
- Clicando em Câmera para tirar uma foto e adicionar à figura.
