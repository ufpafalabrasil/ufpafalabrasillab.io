---
title: "Usuário"
subtitle: VoxLaPS Tutorial
author: Ronaldd Pinho
author_link: https://github.com/pinho
date: 2019-08-10
lastmod: 2019-08-10
type: page
weight: 4
comments: false
#bigimg: [{src: "/img/vox.jpeg", desc: "VoxLaPS: Um Vocalizador Aberto para Plataformas Móveis"}]
---

Instruções para configuração e uso da seção de usuários do app.

<!--more-->

Clique em <b>Usuários</b> na tela inicial.

Na tela "Usuários" é possível ver o usuário padrão chamado "VoxLaPS" criado 
pelo próprio aplicativo, abaixo dele os usuários cadastrados (ou não) 
manualmente, além das opções "Voltar" e "Novo Usuário".

![](/img/voxlaps_manual/inicio-bt-user-m.png)

![](/img/voxlaps_manual/tela-de-user.png)

### Como criar um novo usuário?

Na tela de usuários clique em <b>Novo Usuário</b> para cadastrar um usuário no 
aplicativo.

Será aberta uma tela onde você pode inserir o nome do usuário a ser cadastrado, 
fazer um backup de segurança das informações de usuário ou restaurar um backup 
feito em outra ocasião.

![Tela de Usuarios](/img/voxlaps_manual/3-tela-usuarios.png)    

![Tela de Criação de novo Usuario](/img/voxlaps_manual/4-tela-novousuario.png) 

Para cadastrar o usuário basta digitar o nome no campo indicado e clicar no 
botão verde <b>Salvar</b>.

Após isso, o novo usuário será criado na tela de usuários.

![Criando um novo usuario](/img/voxlaps_manual/criando-user-joao-silva.png) 

![Criando um novo usuario](/img/voxlaps_manual/user-joao-silva-criado.png)  

O botão <b>Backup</b> salva todas as imagens adicionadas pelo usuário em um 
arquivo compactado.

O botão <b>Restore</b> restaura um backup feito anteriormente em outra ocasião.

### Como editar um usuário?

Clicando em um usuário cadastrado ou no usuário VoxLaPS você abrirá a 
<b>tela de usuário</b>, nela clique em <b>Config.</b>.

Clicando em <b>Configurações do Usuário</b> será aberta uma tela onde você pode 
editar o nome do usuário e selecionar umas configurações de varredura como 
tempo (de 1 a 10 segundos) e a cor daborda da varredura.</p>

![Criando um novo usuario](/img/voxlaps_manual/tela-usuario-voxlaps.png) 
![Criando um novo usuario](/img/voxlaps_manual/editando-user-joaosilva.png) 

Altere as opções que desejar e clique em Salvar.

Para excluir o usuário selecionado clique em <b>Remover Usuário</b>.

![Remover usuario](/img/voxlaps_manual/editando-user-joaosilva.png) 

![usuario removido](/img/voxlaps_manual/3-tela-usuarios.png) 
