---
authors:
- Chadia Hosn
- Luiz Baptista
- Tales Imbiriba
- Aldebaro Klautau
type: page
title:     "New resources for Brazilian Portuguese: results for grapheme-to-phoneme and phone classification"
endnote:   "Chadia Hosn, Luiz Baptista, Tales Imbiriba, and Aldebaro Klautau. <b>New resources for Brazilian Portuguese: results for grapheme-to-phoneme and phone classification</b>. <i>In VI International Telecommunications Symposium</i>, 2006."
pub_type:  "article"
journal:   "In VI International Telecommunications Symposium"
year:      "2006"
date:      "2006-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Hosn06a,
    author    = {Chadia Hosn and Luiz Baptista and Tales Imbiriba and Aldebaro Klautau},
    journal   = {In VI International Telecommunications Symposium},
    title     = {New Resources for {B}razilian {P}ortuguese: Results for Grapheme-to-Phoneme and Phone Classification},
    year      = {2006}
}
{{< /highlight >}}
