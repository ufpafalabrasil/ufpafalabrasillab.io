---
authors:
- Nelson Neto
- Carolina Siravenha
- Valquiria Macedo
- Aldebaro Klautau
type: page
title:     "A computer-assisted learning software to help teaching english to brazilians"
endnote:   "Nelson Neto, Carolina Siravenha, Válquiria Macedo, and Aldebaro Klautau. <b>A computer-assisted learning software to help teaching english to brazilians</b>. <i>International Conference on Computational Processing of the Portuguese Language - Special Session</i>, 2008."
pub_type:  "article"
journal:   "International Conference on Computational Processing of the Portuguese Language - Special Session"
year:      "2008"
date:      "2008-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{NelsonCALL08,
    author    = {Nelson Neto and Carolina Siravenha and V\'{a}lquiria Macedo and Aldebaro Klautau},
    journal   = {International Conference on Computational Processing of the Portuguese Language - Special Session},
    title     = {A Computer-Assisted Learning Software to Help Teaching English to Brazilians},
    year      = {2008}
}
{{< /highlight >}}
