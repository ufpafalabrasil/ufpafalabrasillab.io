---
authors:
- N. Jevtić
- A. Klautau
- A. Orlitsky
type: page
title:     "Estimated rank pruning and Java-based speech recognition"
endnote:   "N. Jevtić, A. Klautau, and A. Orlitsky. <b>Estimated rank pruning and Java-based speech recognition</b>. In <i>Automatic Speech Recognition and Understanding Workshop</i>. 2001."
pub_type:  "inproceedings"
booktitle: "Automatic Speech Recognition and Understanding Workshop"
year:      "2001"
date:      "2001-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@inproceedings{Jevtic01,
    author    = {N. Jevti\'c and A. Klautau and A. Orlitsky},
    booktitle = {Automatic Speech Recognition and Understanding Workshop},
    title     = {Estimated rank pruning and {J}ava-based speech recognition},
    year      = {2001}
}
{{< /highlight >}}
