---
authors:
- Nelson Neto
- Carlos Patrick
- Aldebaro Klautau
- Isabel Trancoso
type: page
title:     "Free tools and resources for Brazilian Portuguese speech recognition"
endnote:   "Nelson Neto, Carlos Patrick, Aldebaro Klautau, and Isabel Trancoso. <b>Free tools and resources for Brazilian Portuguese speech recognition</b>. <i>Journal of the Brazilian Computer Society</i>, 17:53-68, 2011."
pub_type:  "article"
journal:   "Journal of the Brazilian Computer Society"
year:      "2011"
date:      "2011-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Neto10,
    author    = {Nelson Neto and Carlos Patrick and Aldebaro Klautau and Isabel Trancoso},
    journal   = {Journal of the Brazilian Computer Society},
    pages     = {53-68},
    title     = {Free tools and resources for {B}razilian {P}ortuguese speech recognition},
    volume    = {17},
    year      = {2011}
}
{{< /highlight >}}
