---
authors:
- Jefferson Morais
- Nelson Neto
- Aldebaro Klautau
type: page
title:     "Tecnologias para o desenvolvimento de sistemas de diálogo falado em Português Brasileiro"
endnote:   "Jefferson Morais, Nelson Neto, and Aldebaro Klautau. <b>Tecnologias para o desenvolvimento de sistemas de diálogo falado em Português Brasileiro</b>. <i>7th Brazilian Symposium in Information and Human Language Technology</i>, 2009."
pub_type:  "article"
journal:   "7th Brazilian Symposium in Information and Human Language Technology"
year:      "2009"
date:      "2009-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Morais09,
    author    = {Jefferson Morais and Nelson Neto and Aldebaro Klautau},
    journal   = {7th Brazilian Symposium in Information and Human Language
Technology},
    title     = {Tecnologias para o Desenvolvimento de Sistemas de Di\'{a}logo Falado em
{P}ortugu\^{e}s {B}rasileiro},
    year      = {2009}
}
{{< /highlight >}}
