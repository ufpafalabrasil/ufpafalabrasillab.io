---
authors:
- A. Klautau
- N. Jevtić
- A. Orlitsky
type: page
title:     "A gentle introduction to maximum mutual information estimation"
endnote:   "A. Klautau, N. Jevtić, and A. Orlitsky. <b>A gentle introduction to maximum mutual information estimation</b>. <i>submitted to IEEE Transactions on Education</i>, 2003."
pub_type:  "article"
journal:   "submitted to IEEE Transactions on Education"
year:      "2003"
date:      "2003-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Klautau03-ieee,
    author    = {A. Klautau and N. Jevti\'c and A. Orlitsky},
    journal   = {submitted to IEEE Transactions on Education},
    title     = {A gentle introduction to maximum mutual information estimation},
    year      = {2003}
}
{{< /highlight >}}
