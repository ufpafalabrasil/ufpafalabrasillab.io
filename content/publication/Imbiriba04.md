---
authors:
- T. Imbiriba
- A. Klautau
- N. Parihar
- S. Raghavan
- J. Picone
type: page
title:     "GMM and kernel-based speaker recognition with the ISIP toolkit"
endnote:   "T. Imbiriba, A. Klautau, N. Parihar, S. Raghavan, and J. Picone. <b>GMM and kernel-based speaker recognition with the ISIP toolkit</b>. In <i>Proceedings of the 2004 IEEE International Workshop on Machine Learning for Signal Processing</i>, 371-380. September 2004."
pub_type:  "inproceedings"
booktitle: "Proceedings of the 2004 IEEE International Workshop on Machine Learning for Signal Processing"
year:      "2004"
date:      "2004-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@inproceedings{Imbiriba04,
    author    = {T. Imbiriba and A. Klautau and N. Parihar and S. Raghavan and J. Picone},
    booktitle = {Proceedings of the 2004 IEEE International Workshop on Machine Learning for Signal Processing},
    city      = {Sao Luis},
    country   = {Brazil},
    month     = {September},
    pages     = {371-380},
    title     = {{GMM} And Kernel-Based Speaker Recognition with the {ISIP} Toolkit},
    year      = {2004}
}
{{< /highlight >}}
