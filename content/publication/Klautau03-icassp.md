---
authors:
- A. Klautau
type: page
title:     "Mining speech: Automatic selection of heterogeneous features using boosting"
endnote:   "A. Klautau. <b>Mining speech: Automatic selection of heterogeneous features using boosting</b>. In <i>ICASSP</i>. 2003."
pub_type:  "inproceedings"
booktitle: "ICASSP"
year:      "2003"
date:      "2003-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@inproceedings{Klautau03-icassp,
    author    = {A. Klautau},
    booktitle = {ICASSP},
    title     = {Mining speech: {A}utomatic selection of heterogeneous features using boosting},
    year      = {2003}
}
{{< /highlight >}}
