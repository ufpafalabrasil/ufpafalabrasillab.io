---
authors:
- A. Klautau
- N. Jevtić
- A. Orlitsky
type: page
title:     "Combined binary classifiers with applications to speech recognition"
endnote:   "A. Klautau, N. Jevtić, and A. Orlitsky. <b>Combined binary classifiers with applications to speech recognition</b>. In <i>ICSLP</i>. 2002."
pub_type:  "inproceedings"
booktitle: "ICSLP"
year:      "2002"
date:      "2002-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@inproceedings{Klautau02,
    author    = {A. Klautau and N. Jevti\'c and A. Orlitsky},
    booktitle = {ICSLP},
    title     = {Combined binary classifiers with applications to speech recognition},
    year      = {2002}
}
{{< /highlight >}}
