---
authors:
- Alberto Abad
- Isabel Trancoso
- Nelson Neto
- Maria Ribeiro
type: page
title:     "Porting an European Portuguese broadcast news recognition system to Brazilian Portuguese"
endnote:   "Alberto Abad, Isabel Trancoso, Nelson Neto, and Maria Ribeiro. <b>Porting an European Portuguese broadcast news recognition system to Brazilian Portuguese</b>. <i>In Interspeech, Brighton, UK</i>, 2009."
pub_type:  "article"
journal:   "In Interspeech, Brighton, UK"
year:      "2009"
date:      "2009-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Trancoso09,
    author    = {Alberto Abad and Isabel Trancoso and Nelson Neto and Maria Ribeiro},
    journal   = {In Interspeech, Brighton, UK},
    title     = {Porting an {E}uropean {P}ortuguese Broadcast News Recognition System to {B}razilian {P}ortuguese},
    year      = {2009}
}
{{< /highlight >}}
