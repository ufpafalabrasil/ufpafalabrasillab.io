---
authors:
- Rafael Oliveira
- Pedro Batista
- Nelson Neto
- Aldebaro Klautau
type: page
title:     "Baseline acoustic models for Brazilian Portuguese using CMU sphinx tools"
endnote:   "Rafael Oliveira, Pedro Batista, Nelson Neto, and Aldebaro Klautau. <b>Baseline acoustic models for Brazilian Portuguese using CMU sphinx tools</b>. <i>12th International Conference on Computational Processing of the Portuguese Language</i>, pages 375-380, 2012."
pub_type:  "article"
journal:   "12th International Conference on Computational Processing of the Portuguese Language"
year:      "2012"
date:      "2012-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{rafael12,
    author    = {Rafael Oliveira and Pedro Batista and Nelson Neto and Aldebaro Klautau},
    journal   = {12th International Conference on Computational Processing of the Portuguese Language},
    pages     = {375-380},
    title     = {Baseline Acoustic Models for {B}razilian {P}ortuguese Using {CMU} Sphinx Tools},
    year      = {2012}
}
{{< /highlight >}}
