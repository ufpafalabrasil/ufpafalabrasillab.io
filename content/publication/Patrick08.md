---
authors:
- Patrick Silva
- Nelson Neto
- Aldebaro Klautau
- Andre Adami
- Isabel Trancoso
type: page
title:     "Speech recognition for Brazilian Portuguese using the Spoltech and OGI-22 corpora"
endnote:   "Patrick Silva, Nelson Neto, Aldebaro Klautau, Andre Adami, and Isabel Trancoso. <b>Speech recognition for Brazilian Portuguese using the Spoltech and OGI-22 corpora</b>. <i>XXVI Simpósio Brasileiro de Telecomunicações</i>, 2008."
pub_type:  "article"
journal:   "XXVI Simpósio Brasileiro de Telecomunicações"
year:      "2008"
date:      "2008-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Patrick08,
    author    = {Patrick Silva and Nelson Neto and Aldebaro Klautau and Andre Adami and Isabel Trancoso},
    journal   = {XXVI Simp\'{o}sio Brasileiro de Telecomunica\c{c}\~{o}es},
    title     = {Speech Recognition for {B}razilian {P}ortuguese using the {S}poltech and {OGI}-22 Corpora},
    year      = {2008}
}
{{< /highlight >}}
