---
authors:
- Cassio Batista
- Thiago Coelho
- Bruno Haick
- Nelson Neto
- Aldebaro Klautau
type: page
title:     "LaPS CSR: a free distributed cloud speech recognition system"
endnote:   "Cassio Batista, Thiago Coelho, Bruno Haick, Nelson Neto, and Aldebaro Klautau. <b>LaPS CSR: a free distributed cloud speech recognition system</b>. In <i>XIX International Scientific Conference for Young Engineers</i>. 2014."
pub_type:  "inproceedings"
booktitle: "XIX International Scientific Conference for Young Engineers"
year:      "2014"
date:      "2014-01-01"
---

## Abstract
This paper describes a cloud speech recognition service based on Julius decoder
running in server mode. The system was set up to recognize speech in Brazilian
Portuguese. The support to the language was developed by the authors with
FalaBrasil research group tools, which are free and available on the group's
site. Julius uses the FalaBrasil cloud to provide online and distributed speech
recognition via Internet. The client side was built on the Android 2.2 platform.
The application can record and send audio, detect the end of the user speech and
listen to the decoder result. To test the system efficiency, recognition time
and accuracy rate were estimated by comparing it to SpeechRecognizer API
provided by Google.

## BibTeX Citation
{{< highlight bibtex >}}
@inproceedings{Batista14,
    author    = {Cassio Batista and Thiago Coelho and Bruno Haick and Nelson Neto and Aldebaro Klautau},
    booktitle = {XIX International Scientific Conference for Young Engineers},
    location  = {Cluj-Napoca, Romania},
    title     = {{LaPS CSR}: A Free Distributed Cloud Speech Recognition System},
    url       = {http://hdl.handle.net/10598/28297},
    year      = {2014}
}
{{< /highlight >}}
