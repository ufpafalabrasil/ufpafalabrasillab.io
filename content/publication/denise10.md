---
authors:
- Denise Alves
- Renan Moura 
- Aldebaro Klautau 
type: page
title:     "Módulo de adaptação de locutor utilizando regressão linear de máxima verossimilhança para sistemas de reconhecimento de voz"
endnote:   "Denise Alves, Renan Moura, and Aldebaro Klautau. <b>Módulo de adaptação de locutor utilizando regressão linear de máxima verossimilhança para sistemas de reconhecimento de voz</b>. <i>XII Workshop de Software Livre</i>, 2010."
pub_type:  "article"
journal:   "XII Workshop de Software Livre"
year:      "2010"
date:      "2010-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{denise10,
    author    = {Denise Alves and Renan Moura and Aldebaro Klautau},
    journal   = {XII Workshop de Software Livre},
    title     = {M\'{o}dulo de Adapta\c{c}\~{a}o de Locutor utilizando Regress\~{a}o Linear de M\'{a}xima Verossimilhan\c{c}a para Sistemas de Reconhecimento de Voz},
    year      = {2010}
}
{{< /highlight >}}
