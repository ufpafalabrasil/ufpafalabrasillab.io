---
authors:
- C. Hosn
- L. A. Baptista
- T. Imbiriba
- A. Klautau
type: page
title:     "New resources for brazilian portuguese: results for grapheme-to-phoneme and phone classification"
endnote:   "C. Hosn, L. A. Baptista, T. Imbiriba, and A. Klautau. <b>New resources for brazilian portuguese: results for grapheme-to-phoneme and phone classification</b>. In <i>2006 International Telecommunications Symposium</i>, volume, 477-482. Sep. 2006. doi:10.1109/ITS.2006.4433322."
pub_type:  "inproceedings"
booktitle: "2006 International Telecommunications Symposium"
year:      "2006"
date:      "2006-01-01"
---

## Abstract
Speech processing is a data-driven technology that relies on public corpora and
associated resources. In contrast to languages such as English, there are few
resources for Brazilian Portuguese (BP). Consequently, there are no publicly
available scripts to design baseline BP systems. This work discusses some
efforts towards decreasing this gap and presents results for two speech
processing tasks for BP: phone classification and grapheme to phoneme (G2P)
conversion. The former task used hidden Markov models to classify phones from
the Spoltech and TIMIT corpora. The G2P module adopted machine learning methods
such as decision trees and was tested on a new BP pronunciation dictionary and
the following languages: British English, American English and French.

## BibTeX Citation
{{< highlight bibtex >}}
@inproceedings{Hosn06,
    author    = {C. {Hosn} and L. A. {Baptista} and T. {Imbiriba} and A. {Klautau}},
    booktitle = {2006 International Telecommunications Symposium},
    doi       = {10.1109/ITS.2006.4433322},
    issn      = {},
    keywords  = {hidden Markov models;learning (artificial intelligence);natural languages;speech processing;Brazilian Portuguese;grapheme-to-phoneme conversion;phone classification;speech processing;data-driven technology;baseline BP system;hidden Markov model;machine learning method;Natural languages;Speech processing;Hidden Markov models;Decision trees;Dictionaries;Speaker recognition;Learning systems;Testing;Speech recognition;Classification tree analysis;Grapheme-to-phoneme;letter-to-sound;decision trees;phone classification;hidden Markov models},
    month     = {Sep.},
    number    = {},
    pages     = {477-482},
    title     = {New resources for Brazilian Portuguese: Results for grapheme-to-phoneme and phone classification},
    volume    = {},
    year      = {2006}
}
{{< /highlight >}}
