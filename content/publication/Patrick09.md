---
authors:
- Patrick Silva
- Nelson Neto
- Aldebaro Klautau
type: page
title:     "Novos recursos e utilização de adaptação de locutor no desenvolvimento de um sistema de reconhecimento de voz para o Português Brasileiro"
endnote:   "Patrick Silva, Nelson Neto, and Aldebaro Klautau. <b>Novos recursos e utilização de adaptação de locutor no desenvolvimento de um sistema de reconhecimento de voz para o Português Brasileiro</b>. <i>In XXVII Simpósio Brasileiro de Telecomunicações</i>, 2009."
pub_type:  "article"
journal:   "In XXVII Simp\'{o}sio Brasileiro de Telecomunicações"
year:      "2009"
date:      "2009-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Patrick09,
    author    = {Patrick Silva and Nelson Neto and Aldebaro Klautau},
    journal   = {In XXVII Simp\'{o}sio Brasileiro de Telecomunica\c{c}\~{o}es},
    title     = {Novos Recursos e Utiliza\c{c}\~{a}o de Adapta\c{c}\~{a}o de Locutor no Desenvolvimento de um Sistema de Reconhecimento de Voz para o {P}ortugu\^{e}s {B}rasileiro},
    year      = {2009}
}
{{< /highlight >}}
