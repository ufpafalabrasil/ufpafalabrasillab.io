---
authors:
- Ana Siravenha
- Nelson Neto
- Valquiria Macedo
- Aldebaro Klautau
type: page
title:     "A computer-assisted learning software using speech synthesis and recognition in brazilian portuguese"
endnote:   "Ana Siravenha, Nelson Neto, Valquiria Macedo, and Aldebaro Klautau. <b>A computer-assisted learning software using speech synthesis and recognition in brazilian portuguese</b>. <i>Interactive Computer Aided Blended Learning</i>, 2009."
pub_type:  "article"
journal:   "Interactive Computer Aided Blended Learning"
year:      "2009"
date:      "2009-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Siravenha09,
    author    = {Ana Siravenha and Nelson Neto and Valquiria Macedo and Aldebaro Klautau},
    journal   = {Interactive Computer Aided Blended Learning},
    title     = {A Computer-assisted Learning Software Using Speech Synthesis and Recognition in Brazilian Portuguese},
    year      = {2009}
}
{{< /highlight >}}
