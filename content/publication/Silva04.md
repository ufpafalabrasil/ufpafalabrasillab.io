---
authors:
- Enio Silva
- Marcus Pantoja
- Jackline Celidônio
- Aldebaro Klautau
type: page
title:     "Modelos de linguagem n-grama para reconhecimento de voz com grande vocabulário"
endnote:   "Enio Silva, Marcus Pantoja, Jackline Celidônio, and Aldebaro Klautau. <b>Modelos de linguagem n-grama para reconhecimento de voz com grande vocabulário</b>. In <i>III Workshop em Tecnologia da Informação e da Linguagem Humana</i>. 2004."
pub_type:  "inproceedings"
booktitle: "III Workshop em Tecnologia da Informação e da Linguagem Humana"
year:      "2004"
date:      "2004-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@inproceedings{Silva04,
    author    = {Enio Silva and Marcus Pantoja and Jackline Celid\^{o}nio and Aldebaro Klautau},
    booktitle = {III Workshop em Tecnologia da Informa\c{c}\~{a}o e da Linguagem Humana},
    city      = {Salvador},
    title     = {Modelos de Linguagem N-grama para Reconhecimento de Voz com Grande Vocabul\'{a}rio},
    year      = {2004}
}
{{< /highlight >}}
