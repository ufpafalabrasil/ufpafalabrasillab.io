---
authors:
- A. Klautau
- N. Jevtić
- A. Orlitsky
type: page
title:     "On nearest-neighbor ECOC with application to all-pairs multiclass SVM"
endnote:   "A. Klautau, N. Jevtić, and A. Orlitsky. <b>On nearest-neighbor ECOC with application to all-pairs multiclass SVM</b>. <i>Journal of Machine Learning Research</i>, 2003."
pub_type:  "article"
journal:   "Journal of Machine Learning Research"
year:      "2003"
date:      "2003-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Klautau03-jmlr,
    author    = {Klautau, A. and Jevti\'c, N. and Orlitsky, A.},
    journal   = {Journal of Machine Learning Research},
    title     = {On nearest-neighbor {ECOC} with application to all-pairs multiclass {SVM}},
    year      = {2003}
}
{{< /highlight >}}
