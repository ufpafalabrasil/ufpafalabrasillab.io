---
authors:
- Pedro Batista
- Patrick Silva
- Nelson Neto
- Aldebaro Klautau
type: page
title:     "A non-visual web-browsing system using speech recognition for Brazilian Portuguese"
endnote:   "Pedro Batista, Patrick Silva, Nelson Neto, and Aldebaro Klautau. <b>A non-visual web-browsing system using speech recognition for Brazilian Portuguese</b>. <i>The International Conference on Computational Processing of Portuguese - Demos Session</i>, 2010."
pub_type:  "article"
journal:   "The International Conference on Computational Processing of Portuguese - Demos Session"
year:      "2010"
date:      "2010-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Pedro10propor,
    author    = {Pedro Batista and Patrick Silva and Nelson Neto and Aldebaro Klautau},
    journal   = {The International Conference on Computational Processing of Portuguese - Demos Session},
    title     = {A non-Visual Web-Browsing System using Speech Recognition for {B}razilian {P}ortuguese},
    year      = {2010}
}
{{< /highlight >}}
