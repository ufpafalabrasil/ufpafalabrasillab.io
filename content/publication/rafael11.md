---
authors:
- Rafael Oliveira
- Pedro Batista
- Nelson Neto
- Aldebaro Klautau
type: page
title:     "Recursos para desenvolvimento de aplicativos com suporte a reconhecimento de voz para desktop e sistemas embarcados"
endnote:   "Rafael Oliveira, Pedro Batista, Nelson Neto, and Aldebaro Klautau. <b>Recursos para desenvolvimento de aplicativos com suporte a reconhecimento de voz para desktop e sistemas embarcados</b>. <i>XII Workshop de Software Livre</i>, 2011."
pub_type:  "article"
journal:   "XII Workshop de Software Livre"
year:      "2011"
date:      "2011-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{rafael11,
    author    = {Rafael Oliveira and Pedro Batista and Nelson Neto and Aldebaro Klautau},
    journal   = {XII Workshop de Software Livre},
    title     = {Recursos para Desenvolvimento de Aplicativos com Suporte a Reconhecimento de Voz para Desktop e Sistemas Embarcados},
    year      = {2011}
}
{{< /highlight >}}
