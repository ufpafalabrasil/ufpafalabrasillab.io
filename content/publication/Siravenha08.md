---
authors:
- Ana Siravenha
- Nelson Neto
- Valquíria Macedo
- Aldebaro Klautau
type: page
title:     "Uso de regras fonológicas com determinação de vogal tônica para conversão grafema-fone em Português Brasileiro"
endnote:   "Ana Siravenha, Nelson Neto, Valquíria Macedo, and Aldebaro Klautau. <b>Uso de regras fonológicas com determinação de vogal tônica para conversão grafema-fone em Português Brasileiro</b>. <i>7th International Information and Telecommunication Technologies Symposium</i>, 2008."
pub_type:  "article"
journal:   "7th International Information and Telecommunication Technologies Symposium"
year:      "2008"
date:      "2008-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Siravenha08,
    author    = {Ana Siravenha and Nelson Neto and Valqu\'{i}ria Macedo and Aldebaro Klautau},
    journal   = {7th International Information and Telecommunication Technologies Symposium},
    title     = {Uso de Regras Fonol\'{o}gicas com Determina\c{c}\~{a}o de Vogal T\^{o}nica para Convers\~{a}o Grafema-Fone em {P}ortugu\^{e}s {B}rasileiro},
    year      = {2008}
}
{{< /highlight >}}
