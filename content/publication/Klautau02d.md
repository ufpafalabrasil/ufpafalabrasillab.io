---
authors:
- A. Klautau
type: page
title:     "Classification of Peterson and Barney's vowels using Weka"
endnote:   "A. Klautau. <b>Classification of Peterson and Barney's vowels using Weka</b>. Technical Report, <i>UFPA, \emph http://www.laps.ufpa.br/aldebaro/papers</i>, 2002."
pub_type:  "techreport"
year:      "2002"
date:      "2002-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@techreport{Klautau02d,
    author      = {A. Klautau},
    institution = {UFPA, \emph{http://www.laps.ufpa.br/aldebaro/papers}},
    title       = {Classification of {P}eterson and {B}arney's vowels using {W}eka},
    year        = {2002}
}
{{< /highlight >}}
