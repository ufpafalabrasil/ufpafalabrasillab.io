---
authors:
- I. Couto
- N. Neto
- V. Tadaiesky
- A. Klautau
- R. Maia
type: page
title:     "An open source HMM-based text-to-speech system for Brazilian Portuguese"
endnote:   "I. Couto, N. Neto, V. Tadaiesky, A. Klautau, and R. Maia. <b>An open source HMM-based text-to-speech system for Brazilian Portuguese</b>. <i>7th International Telecommunications Symposium</i>, 2010."
pub_type:  "article"
journal:   "7th International Telecommunications Symposium"
year:      "2010"
date:      "2010-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Igor10,
    author    = {I. Couto and N. Neto and V. Tadaiesky and A. Klautau and R. Maia},
    journal   = {7th International Telecommunications Symposium},
    title     = {An Open Source {HMM}-based Text-to-Speech System for {B}razilian {P}ortuguese},
    year      = {2010}
}
{{< /highlight >}}
