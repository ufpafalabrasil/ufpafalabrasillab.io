---
authors:
- A. Monte
- D. Ribeiro
- N. Neto
- R. Cruz
- A. Klautau
type: page
title:     "A rule-based syllabification algorithm with stress determination for Brazilian Portuguese natural language processing"
endnote:   "A. Monte, D. Ribeiro, N. Neto, R. Cruz, and A. Klautau. <b>A rule-based syllabification algorithm with stress determination for Brazilian Portuguese natural language processing</b>. <i>17th International Congress of Phonetic Sciences</i>, pages 1418-1421, 2011."
pub_type:  "article"
journal:   "17th International Congress of Phonetic Sciences"
year:      "2011"
date:      "2011-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Monte11,
    author    = {A. Monte and D. Ribeiro and N. Neto and R. Cruz and A. Klautau},
    journal   = {17th International Congress of Phonetic Sciences},
    pages     = {1418-1421},
    title     = {A Rule-based Syllabification Algorithm with Stress Determination for {B}razilian {P}ortuguese Natural Language Processing},
    year      = {2011}
}
{{< /highlight >}}
