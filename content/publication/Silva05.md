---
authors:
- Enio Silva
- Luiz Baptista
- Helane Fernandes
- Aldebaro Klautau
type: page
title:     "Desenvolvimento de um sistema de reconhecimento automático de voz contínua com grande vocabulário para o Português Brasileiro"
endnote:   "Enio Silva, Luiz Baptista, Helane Fernandes, and Aldebaro Klautau. <b>Desenvolvimento de um sistema de reconhecimento automático de voz contínua com grande vocabulário para o Português Brasileiro</b>. <i>XXV Congresso da Sociedade Brasileira de Computação</i>, 2005."
pub_type:  "article"
journal:   "XXV Congresso da Sociedade Brasileira de Computação"
year:      "2005"
date:      "2005-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Silva05,
    author    = {Enio Silva and Luiz Baptista and Helane Fernandes and Aldebaro Klautau},
    journal   = {XXV Congresso da Sociedade Brasileira de Computa\c{c}\~{a}o},
    title     = {Desenvolvimento de um Sistema de Reconhecimento Autom\'{a}tico de Voz Cont\'{i}nua com Grande Vocabul\'{a}rio para o {P}ortugu\^{e}s {B}rasileiro},
    year      = {2005}
}
{{< /highlight >}}
