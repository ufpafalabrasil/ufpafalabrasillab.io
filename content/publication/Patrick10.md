---
authors:
- Patrick Silva
- Pedro Batista
- Nelson Neto
- Aldebaro Klautau
type: page
title:     "An open-source speech recognizer for Brazilian Portuguese with a windows programming interface"
endnote:   "Patrick Silva, Pedro Batista, Nelson Neto, and Aldebaro Klautau. <b>An open-source speech recognizer for Brazilian Portuguese with a windows programming interface</b>. <i>Computational Processing of the Portuguese Language</i>, 6001:128-131, 2010."
pub_type:  "article"
journal:   "Computational Processing of the Portuguese Language"
year:      "2010"
date:      "2010-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Patrick10,
    author    = {Patrick Silva and Pedro Batista and Nelson Neto and Aldebaro Klautau},
    journal   = {Computational Processing of the Portuguese Language},
    pages     = {128-131},
    title     = {An Open-Source Speech Recognizer for {B}razilian {P}ortuguese with a Windows Programming Interface},
    volume    = {6001},
    year      = {2010}
}
{{< /highlight >}}
