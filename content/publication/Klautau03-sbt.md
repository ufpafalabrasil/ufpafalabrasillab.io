---
authors:
- A. Klautau
- N. Jevtić
- A. Orlitsky
type: page
title:     "Speech recognition based on discriminative classifiers"
endnote:   "A. Klautau, N. Jevtić, and A. Orlitsky. <b>Speech recognition based on discriminative classifiers</b>. In <i>SBT, Rio de Janeiro, Brazil</i>. 2003."
pub_type:  "inproceedings"
booktitle: "SBT, Rio de Janeiro, Brazil"
year:      "2003"
date:      "2003-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@inproceedings{Klautau03-sbt,
    author    = {A. Klautau and N. Jevti\'c and A. Orlitsky},
    booktitle = {SBT, Rio de Janeiro, Brazil},
    title     = {Speech recognition based on discriminative classifiers},
    year      = {2003}
}
{{< /highlight >}}
