---
authors:
- A. Klautau
type: page
title:     "A new algorithm for training RBF networks"
endnote:   "A. Klautau. <b>A new algorithm for training RBF networks</b>. In <i>SBT, Rio de Janeiro, Brazil</i>. 2003."
pub_type:  "inproceedings"
booktitle: "SBT, Rio de Janeiro, Brazil"
year:      "2003"
date:      "2003-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@inproceedings{Klautau03-rbf,
    author    = {A. Klautau},
    booktitle = {SBT, Rio de Janeiro, Brazil},
    title     = {A new algorithm for training {RBF} networks},
    year      = {2003}
}
{{< /highlight >}}
