---
authors:
- Cassio Batista
- Thiago Coelho
- Bruno Haick
- Nelson Neto
- Aldebaro Klautau
type: page
title:     "Desenvolvimento e comparação de reconhecedores de fala embarcados e distribuídos para android"
endnote:   "Cassio Batista, Thiago Coelho, Bruno Haick, Nelson Neto, and Aldebaro Klautau. <b>Desenvolvimento e comparação de reconhecedores de fala embarcados e distribuídos para android</b>. In <i>XXXI Brazilian Telecommunications Symposium</i>. 2013."
pub_type:  "inproceedings"
booktitle: "XXXI Brazilian Telecommunications Symposium"
year:      "2013"
date:      "2013-01-01"
resources:
    doi_number: 10.14209/sbrt.2013.219
    research_gate: https://www.researchgate.net/publication/269225899_Desenvolvimento_e_Comparacao_De_Reconhecedores_De_Fala_Embarcados_e_Distribuidos_Para_Android
    link: https://biblioteca.sbrt.org.br/articles/1031
---

## Abstract
Este trabalho compara dois sistemas de reconhecimento de fala que podem ser
usados no desenvolvimento de aplicativos para Android: Julius em modo servidor e
Google. Parte do suporte a Português Brasileiro para o Julius foi desenvolvido
pelos autores no contexto do projeto FalaBrasil. O Julius também utilizou o
servidor do FalaBrasil para prover reconhecimento distribuído via Internet, de
maneira similar ao sistema da empresa Google. São apresentadas comparações entre
os mesmos em termos de taxa de acerto (acurácia) e custo computacional.

## BibTeX Citation
{{< highlight bibtex >}}
@inproceedings{Batista13,
    author    = {Cassio Batista and Thiago Coelho and Bruno Haick and Nelson Neto and Aldebaro Klautau},
    booktitle = {XXXI Brazilian Telecommunications Symposium},
    location  = {Fortaleza, Brazil},
    title     = {Desenvolvimento e Compara\c{c}\~{a}o De Reconhecedores De Fala Embarcados e Distribu\'{i}dos Para Android},
    url       = {http://gestao.sbrt.org.br/simposios/artigo/visualizar/a/325},
    doi       = {10.14209/sbrt.2013.219},
    year      = {2013}
}
{{< /highlight >}}
