---
authors:
- N. Neto
- E. Sousa
- V. Macedo
- A. Adami
- A. Klautau
type: page
title:     "Desenvolvimento de software livre usando reconhecimento e síntese de voz: o estado da arte para o Português Brasileiro"
endnote:   "N. Neto, E. Sousa, V. Macedo, A. Adami, and A. Klautau. <b>Desenvolvimento de software livre usando reconhecimento e síntese de voz: o estado da arte para o Português Brasileiro</b>. <i>6th Fórum Internacional Software Livre</i>, 2005."
pub_type:  "article"
journal:   "6th F\'{o}rum Internacional Software Livre"
year:      "2005"
date:      "2005-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Neto05,
    author    = {N. Neto and E. Sousa and V. Macedo and A. Adami and A. Klautau},
    journal   = {6th F\'{o}rum Internacional Software Livre},
    title     = {Desenvolvimento de Software Livre Usando Reconhecimento e S\'{i}ntese de Voz: O Estado da Arte para o {P}ortugu\^{e}s {B}rasileiro},
    year      = {2005}
}
{{< /highlight >}}
