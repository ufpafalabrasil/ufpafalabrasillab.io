---
authors:
- N. Neto
- P. Silva
- A. Klautau
- A. Adami
type: page
title:     "Spoltech and OGI-22 baseline systems for speech recognition in Brazilian Portuguese"
endnote:   "N. Neto, P. Silva, A. Klautau, and A. Adami. <b>Spoltech and OGI-22 baseline systems for speech recognition in Brazilian Portuguese</b>. <i>International Conference on Computational Processing of Portuguese Language - PROPOR</i>, 2008."
pub_type:  "article"
journal:   "International Conference on Computational Processing of Portuguese Language - PROPOR"
year:      "2008"
date:      "2008-01-01"
---

## Abstract
Unavailable :(

## BibTeX Citation
{{< highlight bibtex >}}
@article{Nelson08,
    author    = {N. Neto and P. Silva and A. Klautau and A. Adami},
    journal   = {International Conference on Computational Processing of Portuguese Language - PROPOR},
    title     = {Spoltech and {OGI}-22 Baseline Systems for Speech Recognition in {B}razilian {P}ortuguese},
    year      = {2008}
}
{{< /highlight >}}
