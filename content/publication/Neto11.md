---
authors:
- Nelson Neto
- Carlos Patrick
- Aldebaro Klautau
- Isabel Trancoso
type: page
title:     "Free tools and resources for brazilian portuguese speech recognition"
endnote:   "Nelson Neto, Carlos Patrick, Aldebaro Klautau, and Isabel Trancoso. <b>Free tools and resources for brazilian portuguese speech recognition</b>. <i>Journal of the Brazilian Computer Society</i>, 17(1):53–68, Mar 2011. "
pub_type:  "article"
journal:   "Journal of the Brazilian Computer Society"
year:      "2011"
date:      "2011-01-01"
---

## Abstract
An automatic speech recognition system has modules that depend on the language and, while there are many public resources for some languages (e.g., English and Japanese), the resources for Brazilian Portuguese (BP) are still limited. This work describes the development of resources and free tools for BP speech recognition, consisting of text and audio corpora, phonetic dictionary, grapheme-to-phone converter, language and acoustic models. All of them are publicly available and, together with a proposed application programming interface, have been used for the development of several new applications, including a speech module for the OpenOffice suite. Performance tests are presented, comparing the developed BP system with a commercial software. The paper also describes an application that uses synthesis and speech recognition together with a natural language processing module dedicated to statistical machine translation. This application allows the translation of spoken conversations from BP to English and vice versa. The resources make easier the adoption of BP speech technologies by other academic groups and industry.

## BibTeX Citation
{{< highlight bibtex >}}
@article{Neto11,
    abstract  = {An automatic speech recognition system has modules that depend on the language and, while there are many public resources for some languages (e.g., English and Japanese), the resources for Brazilian Portuguese (BP) are still limited. This work describes the development of resources and free tools for BP speech recognition, consisting of text and audio corpora, phonetic dictionary, grapheme-to-phone converter, language and acoustic models. All of them are publicly available and, together with a proposed application programming interface, have been used for the development of several new applications, including a speech module for the OpenOffice suite. Performance tests are presented, comparing the developed BP system with a commercial software. The paper also describes an application that uses synthesis and speech recognition together with a natural language processing module dedicated to statistical machine translation. This application allows the translation of spoken conversations from BP to English and vice versa. The resources make easier the adoption of BP speech technologies by other academic groups and industry.},
    author    = {Neto, Nelson and Patrick, Carlos and Klautau, Aldebaro and Trancoso, Isabel},
    day       = {01},
    doi       = {10.1007/s13173-010-0023-1},
    issn      = {1678-4804},
    journal   = {Journal of the Brazilian Computer Society},
    month     = {Mar},
    number    = {1},
    pages     = {53--68},
    title     = {Free tools and resources for Brazilian Portuguese speech recognition},
    url       = {https://doi.org/10.1007/s13173-010-0023-1},
    volume    = {17},
    year      = {2011}
}
{{< /highlight >}}
