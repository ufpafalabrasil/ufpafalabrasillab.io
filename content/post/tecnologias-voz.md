---
title: Tecnologias de Voz
subtitle: Ferramentas para processamento de fala em Português Brasileiro
date: 2019-05-11
#lastmod: 2019-09-03
author: Cassio Batista
#author_link: https://cassota.gitlab.io/
tags: ["ASR", "TTS", "NLP"]
---

O FalaBrasil conta com ferramentas para processamento de texto e desenvolvimento 
de sistemas de reconhecimento e síntese de fala em Português Brasileiro. 

{{< gallery caption-effect="fade" >}}
  {{< figure thumb="-thumb" link="/img/asr.png" caption="ASR: Reconhecimento de fala" alt="Fluxograma de um sistema ASR: conversão de fala para texto." >}}
  {{< figure thumb="-thumb" link="/img/nlp.png" caption="NLP: Processamento de linguagem natural" alt="Sistemas de processamento de texto." >}}
  {{< figure thumb="-thumb" link="/img/tts.png" caption="TTS: Síntese de fala" alt="Fluxograma de um sistema TTS: conversão de texto para fala." >}}
{{< /gallery >}}

<!--more-->

- [ASR: reconhecimento automático de fala](/res/asr/)
- [TTS: síntese de fala](/res/tts/)
- [NLP: processamento de linguagem natural](/res/nlp/)

## Crédito das Imagens
- Orelha:     <a href="http://clipartmag.com/ear-clipart">Ear Clipart</a>     
- Boca:       <a href="http://clipartmag.com/smile-mouth-clipart">Smile Mouth Clipart</a>     
- Computador: <a href="http://clipartmag.com/computer-clipart">Computer Clipart</a>     
- Papel:      <a href="http://clipartmag.com/paper-and-pen-clipart">Paper And Pen Clipart</a>    
<!--EOF-->
