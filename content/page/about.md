---
title: O Grupo FalaBrasil
subtitle: Criação e disponibilização de recursos livres para processamento de linguagem natural e fala em Português Brasileiro
author: Cassio Batista
date: 2019-08-02
lastmod: 2020-03-20
comments: false
---

O FalaBrasil é um grupo de pesquisa criado pelo Laboratório de Processamento de
Sinais ([LaPS](http://www.laps.ufpa.br/)) da Universidade Federal do Pará
([UFPA](https://portal.ufpa.br/)) cujo o objetivo é a criação e disponibilização
de ferramentas e recursos para reconhecimento e síntese de voz em Português
Brasileiro. Atualmente, boa parte dos integrantes do antigo LaPS agora integra o
Núcleo de Pesquisa e Desenvolvimento em Telecomunicações, Automação e Eletrônica
([LASSE](https://www.lasse.ufpa.br/)). O Grupo FalaBrasil, no entanto, faz
agora parte do [LabVIS](http://labvis.ufpa.br/): Laboratório de Visualização,
Interação e Sistemas Inteligentes.
